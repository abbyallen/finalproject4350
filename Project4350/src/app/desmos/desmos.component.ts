import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentService } from '../shared/services/content.service';

@Component({
  selector: 'app-desmos',
  templateUrl: './desmos.component.html',
  styleUrls: ['../calc/calc.component.css']
})
export class DesmosComponent implements OnInit {
  page = Object;

  constructor(private route: ActivatedRoute, private contentService: ContentService) { }

  ngOnInit() {
    const pageData = this.route.snapshot.data['desmos'];
    this.page = this.contentService.pages[pageData];
  }

}
