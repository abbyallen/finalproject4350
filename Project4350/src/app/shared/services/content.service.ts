import { Injectable } from '@angular/core';

@Injectable()
export class ContentService {
  pages: Object = {
    'home': {title: 'Home', content: 'Some home content.'},
    'projects': {title: 'Projects', content: 'Content'},
    'resume': {title: 'Resume', content: 'stufjdsla'},
    'tech': {title: "Technologies Used", content: './page/techused.html'},
    'calc': {title: "Calculus Project", content:'blahblah'},
    'desmos': {title: 'Desmos'},
    'wolfram': {title: 'Wolfram'},
    'geogebra': {title: 'Geogebra'}, 
    'calchome': {title: 'Home'}
    
  };
    constructor() { }
}

