import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentService } from '../shared/services/content.service';

@Component({
  selector: '[app-home]',
  templateUrl: './home.component.html',
  styleUrls: ['../app.component.css']
})
export class PageComponent implements OnInit {
  zip = null;
  current = null;
  temp = null;
  weather = null;
  time = new Date();
  rise = null;
  set = null;
  page = Object;

  constructor(private route: ActivatedRoute, private contentService: ContentService) { }

  ngOnInit() {
    const pageData = this.route.snapshot.data['page'];
    this.page = this.contentService.pages[pageData];

      fetch(`http://api.ipstack.com/check?access_key=2b486f95b7516c2c7103fc921f8bd935`)
        .then(r => r.json())
        .then(json =>{
          this.zip = json.zip;
          
          return fetch(`https://api.openweathermap.org/data/2.5/weather?zip=${this.zip}&APPID=94ca3941ba056d1a2a9efa6825d9fd22`)})
          
        .then(r => r.json())
        .then(json => {
          this.current = json;
          this.set = new Date(this.current.sys.sunset * 1000);
          this.rise = new Date(this.current.sys.sunrise * 1000);

          this.temp = ((Number(this.current.main.temp)-273.15) * (9/5) + 32).toPrecision(2);
          this.weather = this.current.weather[0].main;

          if(this.weather === "Rain"){
            (<HTMLImageElement>document.getElementById("weather")).src = "../assets/rain.png";
          }
          else if (this.weather === "Snow"){
            (<HTMLImageElement>document.getElementById("weather")).src = "../assets/snow.png";
          }
          else if(this.weather === "Clouds" || this.weather === "Fog"){
            (<HTMLImageElement>document.getElementById("weather")).src = "../assets/cloud.png";
          }
          else if(this.set < this.time || this.rise > this.time){
            (<HTMLImageElement>document.getElementById("weather")).src = "../assets/moon.jpg";
          }
          else {
            (<HTMLImageElement>document.getElementById("weather")).src = "../assets/sun.jpg";
          }
        });
  }
}