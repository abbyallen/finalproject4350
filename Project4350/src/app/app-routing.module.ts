import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageComponent } from './home/home.component';
import {ProjectComponent} from './project/project.component';
import {ResumeComponent} from './resume/resume.component';
import {TechComponent} from './tech/tech.component';
import {CalcComponent} from './calc/calc.component';
import {GeogebraComponent} from './geogebra/geogebra.component';
import {WolframComponent} from './wolfram/wolfram.component';
import {DesmosComponent} from './desmos/desmos.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: PageComponent, data: {
      page: 'home'
  }},
  {path: 'projects', component: ProjectComponent, data: {
    page: 'projects'
  }},
  {path: 'tech', component: TechComponent, data: {
    page: 'tech'
  }},
  {path: 'resume', component: ResumeComponent, data: {
    page: 'resume'
  }},
  {path: 'calc', component: CalcComponent, data: {
    page: 'calc'
  }},
  {path: 'desmos', component: DesmosComponent, data: {
    page: 'desmos'
  }},
  {path: 'geogebra', component: GeogebraComponent, data: {
    page: 'geogebra'
  }},
  {path: 'wolfram', component: WolframComponent, data: {
    page: 'wolfram'
  }},
  {path: '**', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
