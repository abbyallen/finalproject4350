import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ContentService } from './shared/services/content.service';
import { PageComponent } from './home/home.component';
import { ProjectComponent } from './project/project.component';
import { TechComponent } from './tech/tech.component';
import { ResumeComponent } from './resume/resume.component';
import {FormsModule} from '@angular/forms';
import { CalcComponent } from './calc/calc.component';
import { DesmosComponent } from './desmos/desmos.component';
import { WolframComponent } from './wolfram/wolfram.component';
import { GeogebraComponent } from './geogebra/geogebra.component'


@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    ProjectComponent,
    TechComponent,
    ResumeComponent,
    CalcComponent,
    DesmosComponent,
    WolframComponent,
    GeogebraComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ContentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
