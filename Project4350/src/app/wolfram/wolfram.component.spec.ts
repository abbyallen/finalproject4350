import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WolframComponent } from './wolfram.component';

describe('WolframComponent', () => {
  let component: WolframComponent;
  let fixture: ComponentFixture<WolframComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WolframComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WolframComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
