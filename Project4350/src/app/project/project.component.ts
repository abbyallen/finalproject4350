import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentService } from '../shared/services/content.service';


@Component({
  selector: 'app-page',
  templateUrl: './project.component.html',
  styleUrls: ['../app.component.css']
})
export class ProjectComponent implements OnInit {
  page = Object;

  constructor(private route: ActivatedRoute, private contentService: ContentService) { }

  ngOnInit() {
    const pageData = this.route.snapshot.data['page'];
    this.page = this.contentService.pages[pageData];
  }

}