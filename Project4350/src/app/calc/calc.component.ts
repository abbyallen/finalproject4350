import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentService } from '../shared/services/content.service';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['/calc.component.css']
})
export class CalcComponent implements OnInit {
  page=Object;

  constructor(private route: ActivatedRoute, private contentService: ContentService) { }

  ngOnInit() {
    const pageData = this.route.snapshot.data['calc'];
    this.page = this.contentService.pages[pageData];
  }

}
